# KISS RB

## Keep It Simple, Stupid... in Ruby.

Kiss-rb is meant to be a toolbox of dead simple tools. For Ruby.

## Tools

### CLI (Command Line Interface)

* [OPC](https://gitlab.com/kiss-rb/opc) - Option Parser with Completion

### Idea

One time I decided to build a piece of a furniture by myself, I went to my toolbox to get few tools. In my messy toolbox, I couldn't find one crucial tool. I was unable to build. Later that day I found the tool and it came to me:

> There is no difference between not having a tool and having it in a messy box.

Ruby is a language that I like the most. I use to thing that I like Ruby because it gives endless possibilities. But from endless possibilities you can not build anything. Creation requires constrains, constrains that brings order. Now I understand that Ruby is not about possibilities but rather about freedom of choosing your own constrains.

My constrain of choice is **simplicity**. I would rather repeat myself from time to time if it leads to simpler solution.

> Tool that is to difficult to comprehend is no different from tool that does not exist. 
